package pm.c7.chestsearch;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

@Mod(modid = ChestSearcher.MODID, version = ChestSearcher.VERSION)
@SideOnly(Side.CLIENT)
public class ChestSearcher {
    public static final String MODID = "chestsearcher";
    public static final String VERSION = "0.0.1";

    public static ChestSearcher INSTANCE = new ChestSearcher();

    private Minecraft mc;
    private ResourceLocation searchBar;
    private GuiTextField searchField;
    private boolean skip;

    public ChestSearcher() {
        this.mc = Minecraft.getMinecraft();
        this.searchBar = new ResourceLocation(MODID, "search_bar.png");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(INSTANCE);
    }

    @SubscribeEvent
    public void onInitGui(GuiScreenEvent.InitGuiEvent.Post event) {
        GuiScreen gui = event.gui;
        String query = "";

        if (this.searchField != null) {
            query = this.searchField.getText();
        }

        if (gui instanceof GuiChest) {
            FontRenderer fontRenderer = this.mc.fontRendererObj;
            gui.allowUserInput = true;

            int i = 186;
            int j = i - 108;
            int rows = (((GuiChest) gui).inventorySlots.inventorySlots.size() - 12) / 9;

            int guiLeft = (gui.width - 176) / 2;
            int guiTop = (gui.height - (j + rows * 18)) / 2;

            (this.searchField = new GuiTextField(0, fontRenderer, guiLeft + 176 - 90 - 8 + 2, guiTop + 6, 86, fontRenderer.FONT_HEIGHT)).setText(query);
            this.searchField.setMaxStringLength(50);
            this.searchField.setEnableBackgroundDrawing(false);
            this.searchField.setTextColor(16777215);
            this.searchField.setFocused(false);
            this.searchField.setCanLoseFocus(true);
            this.searchField.setVisible(true);
        } else {
            this.searchField = null;
        }
    }

    @SubscribeEvent
    public void onCharTyped(final GuiScreenEvent.KeyboardInputEvent.Pre event) {
        if (this.searchField != null) {
            char key = Keyboard.getEventCharacter();
            int keyCode = Keyboard.getEventKey();

            if (this.skip) {
                this.skip = false;
            } else if (this.searchField.isFocused()) {
                if (this.searchField.textboxKeyTyped(key, keyCode)) {
                    for (int i = 0; i < 9; ++i) {
                        if (this.mc.gameSettings.keyBindsHotbar[i].isPressed()) {
                            event.setCanceled(true);
                        }
                    }
                    event.setCanceled(true);
                }

                if (keyCode == 69 || (keyCode >= 262 && keyCode <= 265)) {
                    final int cursorPosition = this.searchField.getCursorPosition();
                    final int selectionEnd = this.searchField.getSelectionEnd();
                    event.setCanceled(true);
                    switch (keyCode) {
                        case 262: {
                            if (GuiScreen.isShiftKeyDown()) {
                                if (GuiScreen.isCtrlKeyDown()) {
                                    this.searchField.setSelectionPos(this.searchField.getNthWordFromPos(1, this.searchField.getSelectionEnd()));
                                    break;
                                }
                                this.searchField.setSelectionPos(this.searchField.getSelectionEnd() + 1);
                                break;
                            } else {
                                if (GuiScreen.isCtrlKeyDown()) {
                                    this.searchField.setCursorPosition(this.searchField.getNthWordFromCursor(1));
                                    break;
                                }
                                if (selectionEnd > cursorPosition) {
                                    this.searchField.setCursorPosition(selectionEnd);
                                    break;
                                }
                                if (cursorPosition > selectionEnd) {
                                    this.searchField.setCursorPosition(cursorPosition);
                                    break;
                                }
                                this.searchField.moveCursorBy(1);
                                break;
                            }
                        }
                        case 263: {
                            if (GuiScreen.isShiftKeyDown()) {
                                if (GuiScreen.isCtrlKeyDown()) {
                                    this.searchField.setSelectionPos(this.searchField.getNthWordFromPos(-1, this.searchField.getSelectionEnd()));
                                    break;
                                }
                                this.searchField.setSelectionPos(this.searchField.getSelectionEnd() - 1);
                                break;
                            } else {
                                if (GuiScreen.isCtrlKeyDown()) {
                                    this.searchField.setCursorPosition(this.searchField.getNthWordFromCursor(-1));
                                    break;
                                }
                                if (selectionEnd < cursorPosition) {
                                    this.searchField.setCursorPosition(selectionEnd);
                                    break;
                                }
                                if (cursorPosition < selectionEnd) {
                                    this.searchField.setCursorPosition(cursorPosition);
                                    break;
                                }
                                this.searchField.moveCursorBy(-1);
                                break;
                            }
                        }
                        case 264: {
                            this.searchField.setCursorPositionEnd();
                            break;
                        }
                        case 265: {
                            this.searchField.setCursorPositionZero();
                            break;
                        }
                    }
                    return;
                }
            } else if (this.mc.gameSettings.keyBindChat.isPressed()) {
                this.searchField.setFocused(true);
                event.setCanceled(true);
                this.skip = true;
            }
        }
    }

    @SubscribeEvent
    public void onForeground(GuiScreenEvent.DrawScreenEvent.Post event) {
        if (this.searchField != null) {
            GlStateManager.disableLighting();
            this.mc.getTextureManager().bindTexture(this.searchBar);
            GlStateManager.color(1, 1, 1, 1);
            Gui.drawModalRectWithCustomSizedTexture(this.searchField.xPosition - 2, this.searchField.yPosition - 2, 0.0f, 0.0f, 90, 12, 90.0f, 12.0f);
            this.searchField.drawTextBox();
            if (!this.searchField.getText().isEmpty() && !this.searchField.getText().equals("")) {
                for (Slot s : ((GuiContainer)event.gui).inventorySlots.inventorySlots) {
                    if (!(s.inventory instanceof InventoryPlayer)) {
                        ItemStack stack = s.getStack();
                        if (this.stackMatches(this.searchField.getText(), stack)) {
                            continue;
                        }

                        int i = 186;
                        int j = i - 108;
                        int rows = (((GuiChest) event.gui).inventorySlots.inventorySlots.size() - 12) / 9;

                        int guiLeft = (event.gui.width - 176) / 2;
                        int guiTop = (event.gui.height - (j + rows * 18)) / 2;

                        int x = guiLeft + s.xDisplayPosition;
                        int y = guiTop + s.yDisplayPosition;

                        GlStateManager.disableDepth();
                        Gui.drawRect(x, y, x + 16, y + 16, -2130771968);
                        GlStateManager.enableDepth();
                    }
                }
            }
            GlStateManager.enableLighting();
        }
    }

    @SubscribeEvent
    public void onMouseClicked(final GuiScreenEvent.MouseInputEvent.Pre event) {
        if (this.searchField != null) {
            int mouseX = Mouse.getEventX() * event.gui.width / this.mc.displayWidth;
            int mouseY = event.gui.height - Mouse.getEventY() * event.gui.height / this.mc.displayHeight - 1;
            int mouseBtn = Mouse.getEventButton();

            if (Mouse.getEventButtonState())
                this.searchField.mouseClicked(mouseX, mouseY, mouseBtn);
        }
    }

    private boolean stackMatches(String text, ItemStack stack) {
        if (stack == null) {
            return false;
        }
        ArrayList<String> keys = new ArrayList<String>();
        keys.add(stack.getDisplayName());
        keys.add(stack.getUnlocalizedName());
        if (stack.isItemEnchanted()) {
            for (Map.Entry<Integer, Integer> e : EnchantmentHelper.getEnchantments(stack).entrySet()) {
                keys.add(Enchantment.getEnchantmentById(e.getKey()).getTranslatedName(e.getValue()));
            }
        }
        for (String key : keys) {
            if (key.toLowerCase(Locale.ROOT).contains(text.toLowerCase(Locale.ROOT))) {
                return true;
            }
        }
        return false;
    }
}